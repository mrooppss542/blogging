from flask import Flask,render_template,request,redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///posts.db' #It is used to mention the path where the database is stored.///(from where this file is there) is relative path //// is absolute path(from root).
db=SQLAlchemy(app) #Now we connected Sqlalchemy with app and we can use database.
#we create databasetable like modelStructure of database.
class BlogPost(db.Model):
    id=db.Column(db.Integer,primary_key=True) #Captial C for Columns
    title=db.Column(db.Text,nullable=False)
    description=db.Column(db.Text,nullable=False)
    author=db.Column(db.String(20),nullable=False,default='N/A')
    date_posted=db.Column(db.DateTime,nullable=False,default=datetime.utcnow())

    def __repr__(self): #reprint like it is going to print out after we create a new table(data) like new blogpost.
        return 'Blog Post' + str(self.id)

'''all_posts=[
    {
        'title' : 'Post1',
        'author' : 'Karthik',
        'description' : 'This is my content'
    },
    {
        'title' : 'Post2',
        'description' : 'This is my content'
    },
    {
        'title' : 'Post3',
        'author' : 'Madhu',
        'description' : 'This is my content'
    }
]'''

@app.route('/') #The base directory
def index():
    return render_template('index.html') # we can also return '''All html code'''But we use templates to create seperated html files and include them here.
@app.route('/allposts',methods=['GET','POST']) #nothing is mentioned it only accepts GET requests.
def allposts(): 
    if request.method == 'POST':
        post_title=request.form['title']
        content=request.form['content']
        post_author=request.form['author']
        new_blogpost=BlogPost(title=post_title,description=content,author=post_author)
        db.session.add(new_blogpost) #This will be there only for this runtime.
        db.session.commit() #This will be there permanently.
        return redirect('/allposts')
    else:
        #overriding the above allposts list dictionary.
        all_posts=BlogPost.query.order_by(BlogPost.date_posted).all()
        return render_template('posts.html',Posts=all_posts)
@app.route('/allposts/delete/<int:id>')
def delete(id):
    post=BlogPost.query.get_or_404(id) #If the blog post with that id doesnot exit the code doesnot break.
    db.session.delete(post)
    db.session.commit()
    return redirect('/allposts')
@app.route('/allposts/edit/<int:id>',methods=['POST','GET'])
def edit(id):
    post=BlogPost.query.get_or_404(id) #If the blog post with that id doesnot exit the code doesnot break.
    if request.method == 'POST':
        post=BlogPost.query.get_or_404(id) #If the blog post with that id doesnot exit the code doesnot break.
        post.title=request.form['title']
        post.author=request.form['author']
        post.description=request.form['content']
        db.session.commit()
        return redirect('/allposts')
    else:
        return render_template('edit.html',post=post)
@app.route('/allposts/new',methods=['POST','GET'])
def new_post():
    if request.method == 'POST':
        post_title=request.form['title']
        content=request.form['content']
        post_author=request.form['author']
        new_blogpost=BlogPost(title=post_title,description=content,author=post_author)
        db.session.add(new_blogpost) #This will be there only for this runtime.
        db.session.commit() #This will be there permanently.
        return redirect('/allposts')
    else:
        return render_template('new_post.html')
if __name__ == "__main__":
    app.run(debug=True)